import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * A lobster. Lobsters live on the beach. They like to eat crabs. (Well, in our game
 * they do...)
 * 
 * The lobster walks around randomly. If it runs into a crab it eats it.
 * In this version, we have added a sound effect, and the game stops when
 * a lobster eats the crab.
 * 
 * @author your_username@email.uscb.edu
 * @version 145fa18_hw3_solution
 */
public class Lobster extends Actor
{
    
    /* FIELDS */
    private GreenfootImage image1;
    private GreenfootImage image2;
    
    /*CONSTRUCTORS */
    public Lobster()
    {
        image1 = new GreenfootImage ( "lobster.png" );
        image2 = new GreenfootImage ( "lobster2.png" );
        
        setImage( image1 ); // Lobster starts out red
    } // end Lobster constructor
    
    /**
     * Do whatever lobsters do.
     */
    public void act()
    {
        turnAtEdge();
        randomTurn();
        randomTurnTowardsCenter(); // for Exercise 4.28
        move(5);
        
        CrabWorld myCrabWorld = (CrabWorld)getWorld();
        if ( myCrabWorld.getCrabDefenseStatus() )
        {
         lookForCrab();   
        }
        else //otherwise, the crab is NOT on defense (i.e., it is on offense)
        {
            setImage( image2 ); // the lobster is BLUE when the crab is on offense
        } //end if/else
        lookForCrab();
    } // end method act

    /**
     * Check whether we are at the edge of the world. If we are, turn a bit.
     * If not, do nothing.
     */
    public void turnAtEdge()
    {
        if ( isAtEdge() ) 
        {
            turn(17);
        } // end if
    } // end method turnAtEdge

    /**
     * Randomly decide to turn from the current direction, or not. If we turn
     * turn a bit left or right by a random degree.
     */
    public void randomTurn()
    {
        if (Greenfoot.getRandomNumber(100) < 10) 
        {
            turn(Greenfoot.getRandomNumber(91)-45);
        } // end if
    } // end method randomTurn

    /**
     * For Exercise 4.28:
     * 
     * Randomly decide to turn towards the center of the screen (where x = 280, y = 280).
     * Note that if this happens too often, the lobsters will all converge in the middle of
     * the screen
     */
    public void randomTurnTowardsCenter()
    {
        if ( Greenfoot.getRandomNumber(100) < 2 )
        {
            turnTowards( 280, 280);
        } // end if
    } // end method randomTurnTowardsCenter

    /**
     * Try to pinch a crab. That is: check whether we have stumbled upon a crab.
     * If we have, remove the crab from the game, and stop the program running.
     */
    public void lookForCrab()
    {
        if ( isTouching(Crab.class) ) 
        {
            removeTouching(Crab.class);
            Greenfoot.playSound("au.wav");
            Greenfoot.stop();
        } // end if
    } // end method lookForCrab
} // end class Lobster