import greenfoot.*;  // imports Actor, World, Greenfoot, GreenfootImage

/**
 * The CrabWorld is the place where crabs and other creatures live. 
 * It creates the initial population.
 *  
 * @author your_username@email.uscb.edu
 * @version 145fa19_hw3_solution
 */
public class CrabWorld extends World
{
    /*
     * FIELD(S)
     */
    /*
     * For Exercise 4.31, add a timeCounter to the CrabWorld
     * (it makes more sense for the CrabWorld to keep track of time...
     * it's not like the crab is wearing a watch. Right?)
     */
    private int timeCounter;

    /* for Lab 6 */
    private boolean crabDefenseStatus; // Crabworld "has-a" crabDefenseStatus
    private Crab myCrab;               // Crabworld "has-a" Crab player-character
                                       // IMPORTANT: All instance variables (and other
                                       // fields) have class-wide scope
                                       
    /* for ICE on 4 Dec 2019 */
    /*
    private Lobster lobster;
    private Lobster lobster2;
    private Lobster lobster3;
    */
    private Lobster[] lobsters = new Lobster [3]; // declare an EMPTY, 1-D array CAPABLE
                                                  // of storing REFERENCES to 3 Lobster objects
    
    private int[] lobsterXcoords = { 334, 481, 79}; // this array uses an ARRAY INITIALIZER
                                                    // to both declare a 3-element array of ints
                                                    // AND then populate the array with int values
    private int[] lobsterYcoords = { 65, 481, 270};
    /*
     * CONSTRUCTOR(S)
     */
    /**
     * Create the crab world (the beach). Our world has a size 
     * of 560x560 cells, where every cell is just 1 pixel.
     */
    public CrabWorld() 
    {
        // a call to the superclass constructor (if needed) must come before
        // any other statements in the constructor
        super(560, 560, 1);

        // For Ex. 4.31, initialize timeCounter for a COUNTDOWN (As opposed to
        // simply counting the number of act cycles that have transpired
        timeCounter = 500;
        crabDefenseStatus = true; // the crab is on DEFENSE at the start of the game
        // prepare the world
        prepare();
    }

    /*
     * METHOD(S)
     */    
    /**m
     * Enables the CrabWorld to update its state from one cycle to the next
     */
    public void act()
    {

        // Ex 4.32: Decrement timeCounter
        timeCounter = timeCounter -= 1; // this is the equivalent to timeCounter = timeCounter - 1;
        // (also has the same effect as timeCounter--;)
        /*  
         * For Exercises 4.33 and 4.34:
         * the showText method takes 3 arguments:
         * 1) a string for a text message to be displayed on the 
         *    screen at the position indicated by the 
         *    following values:
         *    2) an int x-coordinate
         *    3) an int y-coordinate
         */
        showText("Time left: " + timeCounter, 100, 40);

        // Also for Ex. 4.32:
        // If the timeCounter reaches zero, play a "time is up" sound
        // and end the game. Note that using <= operator (instead of ==)
        // helps to "bulletproof" our code somewhat against the possibility 
        // of timeCounter "skipping over" zero.
        if (timeCounter <= 0)
        {
            showText (  "Time's up!", 100, 40 ); // not required; call it a "bonus"
            Greenfoot.playSound("alarm.mp3");
            Greenfoot.stop();
        } // end if statement
    } // end method act

    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare()
    {
        // add the crab
        // Crab crab = new Crab();
        myCrab = new Crab();
        addObject(myCrab, 231, 203);

        // We want to generate the int values for the x- and y- coordinates
        // such that they are within the ranges x = { 25 ... 535 }
        // and y = { 25 ... 535 }

        /*
        int wormIndex = 0; // initialize counter variable
        int numberOfWorms = 10; // number of iterations
        // if you don't know how many iterations you need,
        // (that is, if your code uses "indefinite iteration")
        // then a while loop may be your best choice
        while ( wormIndex < numberOfWorms ) {
        addObject( new Worm(),Greenfoot.getRandomNumber(511) + 25,Greenfoot.getRandomNumber(511) + 25);
        wormIndex = wormIndex + 1; // update counter variable
        } // end while
         */
        
        /*
        int numberOfWorms = 10;
        // Rule of thumb: if you know in advance how many times
        // your loop needs to repeat (i.e., define repitition),
        // then the common practice is to use a counter-controlled
        // for loop
        for ( int wormIndex = 0 ; wormIndex < numberOfWorms ; wormIndex = wormIndex++ )
        {
            addObject( new Worm(),Greenfoot.getRandomNumber(511) + 25,Greenfoot.getRandomNumber(511) + 25);
        } // end for
        */
        
        int wormIndex = 0; // initialize counter variable
        int numberOfWorms = 10; // number of iterations

        do {
            addObject( new Worm(),Greenfoot.getRandomNumber(511) + 25,Greenfoot.getRandomNumber(511) + 25);
            wormIndex = wormIndex + 1; // update counter variable
        } while ( wormIndex < numberOfWorms ); // Note: in a do...while loop, the loop-continuation condition
                                               // is evaluated at the END of the loop,
                                               // so that you are guranteed at least ONE interation
        /*
        Worm worm = new Worm();
        addObject(worm,Greenfoot.getRandomNumber(511) + 25,Greenfoot.getRandomNumber(511) + 25);
        Worm worm2 = new Worm();
        addObject(worm2,Greenfoot.getRandomNumber(511) + 25,Greenfoot.getRandomNumber(511) + 25);
        Worm worm3 = new Worm();
        addObject(worm3,Greenfoot.getRandomNumber(511) + 25,Greenfoot.getRandomNumber(511) + 25);
        Worm worm4 = new Worm();
        addObject(worm4,Greenfoot.getRandomNumber(511) + 25,Greenfoot.getRandomNumber(511) + 25);
        Worm worm5 = new Worm();
        addObject(worm5,Greenfoot.getRandomNumber(511) + 25,Greenfoot.getRandomNumber(511) + 25);
        Worm worm6 = new Worm();
        addObject(worm6,Greenfoot.getRandomNumber(511) + 25,Greenfoot.getRandomNumber(511) + 25);
        Worm worm7 = new Worm();
        addObject(worm7,Greenfoot.getRandomNumber(511) + 25,Greenfoot.getRandomNumber(511) + 25);
        Worm worm8 = new Worm();
        addObject(worm8,Greenfoot.getRandomNumber(511) + 25,Greenfoot.getRandomNumber(511) + 25);
        Worm worm9 = new Worm();
        addObject(worm9,Greenfoot.getRandomNumber(511) + 25,Greenfoot.getRandomNumber(511) + 25);
        Worm worm10 = new Worm();
        addObject(worm10,Greenfoot.getRandomNumber(511) + 25,Greenfoot.getRandomNumber(511) + 25);
         */

        // add the three lobsters
        
        lobsters[0] = new Lobster();
        addObject(lobsters[0], lobsterXcoords[0], lobsterYcoords[0] );
        lobsters[1] = new Lobster();
        addObject(lobsters[1], lobsterXcoords[1], lobsterYcoords[1] );
        lobsters[2] = new Lobster();
        addObject(lobsters[2], lobsterXcoords[2], lobsterYcoords[2] );
        
        
        
        // consolidate the above redundant statements into a counter-controlled for loop
        /*
        for ( int lobsterIndex = 0; lobsterIndex < 3 ; lobsterIndex++ )
        {
            lobsters[0] = new Lobster();
            addObject(lobsters[lobsterIndex], lobsterXcoords[lobsterIndex], lobsterYcoords[lobsterIndex] );
        } // end for
        */
    } // end method prepare

    /**
     * This is an an example of a mutator ("setter") method
     */
    public void setCrabDefenseStatus(  )
    {
        /*
         * The keyword 'this' is used to disambiguate between 
         * an instance variable and a local variable of the same name
         * (this.crabDefenseStatus <-- The instance variable)
         */
        this.crabDefenseStatus = crabDefenseStatus;
    } // end method setCrabDefenseStatus

    /**
     * This is an example of an accessor ("getter") method
     */
    public boolean getCrabDefenseStatus()
    {
        return crabDefenseStatus;
    } // end method getCrabDefenseStatus

} // end class CrabWorld