
import greenfoot.*;

/**
 * This class defines a crab. Crabs live on the mountains.
 */
public class Crab extends Actor
{
    /* INSTANCE VARIABLES */
    private GreenfootImage image1; // image1 can be accessed anywhere in the class
    private GreenfootImage image2; // image2 can be accessed anywhere in the class
    private int wormsEaten; // instance variables will have a default value
    private int cycleCount; // this keeps track of the number of cycles through 
    // the act method that have transpired since the
    // crab's image was last updated

    /* CONSTRUCTORS */
    /**
     *  Initialize a newly-instantiated crab object with
     *  its two possible images; then set the intial image to 
     *  one of those images.
     */
    public Crab()
    {
        image1 = new GreenfootImage("crab.png");
        image2 = new GreenfootImage("crab2.png");
        setImage(image1);
        wormsEaten = 0;
        cycleCount = 0;
    } // end Crab constructor

    /* METHODS */
    /**
     * Act - do whatever the crab wants to do. This method 
     * is called whenever the 'Act' or 'Run' button 
     * gets pressed in the environment.
     */
    public void act()
    {
        checkKeypress();
        move(5); 
        lookForWorm(); 
        switchImage();
    }// end method act

    /**
     * Checks for keys pressed to conrol the movement of the crab.
     */
    public void checkKeypress()
    {
        if ( Greenfoot.isKeyDown("left") )
        {
            turn(-4);
        } // end if

        if ( Greenfoot.isKeyDown("right") )
        {
            turn(4);
        } // end if 
    } // end method checkKeyPress

    /**
     * Check whether we have stumbled upon a worm.
     * If we have, eat it. If not, do nothing.
     */
    public void lookForWorm() // this construct DEFINES the lookForWorm method
    {
        if ( isTouching(Worm.class) )
        {
            removeTouching (Worm.class);
            Greenfoot.playSound("slurp.wav");

            wormsEaten += 1;//wormsEaten = wormsEaten + 1;

            if (wormsEaten ==8)
            {
                Greenfoot.playSound("fanfare.wav");
                Greenfoot.stop();
            } // end INNER if
        } //end OUTER if
    }//end method lookForWorm

    /**
     * If the crab reaches the edge of the screen,
     * then have it turn before moving again.
     */
    public void turnAtEdge()
    {
        if ( isAtEdge() )
        {
            turn(17);   
        } // end if
    } // end method turnAtEdge

    /**
     * Make the crab turn, at random, ten percent of the time.
     * When the crab turns, it will turn by some random angle
     * between -45 degrees and +45 degrees
     */
    public void randomTurn()
    {
        if (Greenfoot.getRandomNumber(100) < 10)
        {
            turn( Greenfoot.getRandomNumber(91) - 45); 
        }//end if
    } // end method randomTurn

    /**
     * Alternates the crab's image between image1 and image2
     */
    public void switchImage()
    { 
        if ( getImage() == image1 )
        {
            setImage (image2);
            
            cycleCount = cycleCount + 1;
            if (cycleCount == 2000)
            {
                setImage(image1);
                cycleCount = 0;
            }
        }
        
    } // end method switchImage
}// end class Crab
