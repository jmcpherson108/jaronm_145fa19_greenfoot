import greenfoot.*;  // (Actor, World, Greenfoot, GreenfootImage)

public class CrabWorld extends World
{
    /**
     * Create the crab world (the beach). Our world has a size 
     * of 560x560 cells, where every cell is just 1 pixel.
     */
    public CrabWorld() 
    {
        // set the size resolution of the world
        // by first initilaizing the "World" object
        super(560, 560, 1);
        /*
        // Create a new object of the Crab class and
        // then assign a reference (to that object) to a variable
        Crab myCrab = new Crab();

        // Add the object
        // to the world at the indicated coordinates
        addObject( myCrab, 250, 200 );

        // Exercise 4.11: Create 3 lobsters and then place each
        // of them in the Crabworld
        Lobster lobster1 = new Lobster();
        addObject( lobster1, 400, 400);

        Lobster lobster2 = new Lobster();
        addObject( lobster2, 500, 100);

        Lobster lobster3 = new Lobster();
        addObject( lobster3, 100, 350);

        // Exercise 4.12: Create and place two worms in the Crabworld
        Worm worm1 = new Worm();
        addObject( worm1, 280, 125);

        Worm worm2 = new Worm();
        addObject( worm2, 180, 500)
         */

        prepare();
    } //end CrabWorld constructor
    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        Crab crab = new Crab();
        addObject(crab,290,216);
        
        Lobster lobster = new Lobster();
        addObject(lobster,367,360);
        
        Lobster lobster2 = new Lobster();
        addObject(lobster2,472,39);
        
        Lobster lobster3 = new Lobster();
        addObject(lobster3,122,362);
        
        // We want to generate int values for the x- and y- coordinates
        // such that they are within the ranges x= { 25 ... 535 }
        // and y = { 25 ... 535 }
        
        
        Worm worm = new Worm();
        addObject(worm,Greenfoot.getRandomNumber(511) + 25, Greenfoot.getRandomNumber( 511 ) +25 );
        Worm worm2 = new Worm();
        addObject(worm2,Greenfoot.getRandomNumber(511) + 25, Greenfoot.getRandomNumber( 511 ) +25 );
        Worm worm3 = new Worm();
        addObject(worm3,Greenfoot.getRandomNumber(511) + 25, Greenfoot.getRandomNumber( 511 ) +25 );
        Worm worm4 = new Worm();
        addObject(worm4,Greenfoot.getRandomNumber(511) + 25, Greenfoot.getRandomNumber( 511 ) +25 );
        Worm worm5 = new Worm();
        addObject(worm5,Greenfoot.getRandomNumber(511) + 25, Greenfoot.getRandomNumber( 511 ) +25 );
        Worm worm6 = new Worm();
        addObject(worm6,Greenfoot.getRandomNumber(511) + 25, Greenfoot.getRandomNumber( 511 ) +25 );
        Worm worm7 = new Worm();
        addObject(worm7,Greenfoot.getRandomNumber(511) + 25, Greenfoot.getRandomNumber( 511 ) +25 );
        Worm worm8 = new Worm();
        addObject(worm8,Greenfoot.getRandomNumber(511) + 25, Greenfoot.getRandomNumber( 511 ) +25 );
        Worm worm9 = new Worm();
        addObject(worm9,Greenfoot.getRandomNumber(511) + 25, Greenfoot.getRandomNumber( 511 ) +25 );
        Worm worm10 = new Worm();
        addObject(worm10,Greenfoot.getRandomNumber(511) + 25, Greenfoot.getRandomNumber( 511 ) +25 );
    }
} // end class CrabWorld

