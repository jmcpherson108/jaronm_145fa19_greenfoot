import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Lobster here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Lobster extends Actor
{
    /**
     * Act - do whatever the Lobster wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        turnAtEdge();
        randomTurn();
        move(5);
        lookForCrab();
    } // end method act

    /**
     * Check whether we have stumbled upon a crab.
     * If we have, eat it. If not, do nothing.
     */
    public void lookForCrab() // this construct DEFINES the lookForCrab method
    {
        if ( isTouching(Crab.class) )
        {
            removeTouching (Crab.class);
            Greenfoot.playSound ("au.wav");
            Greenfoot.stop();
        } //end if
    }//end method lookForCrab

    /**
     * If the Lobser reaches the edge of the screen,
     * then have it turn before moving again.
     */
    public void turnAtEdge()
    {
        if ( isAtEdge() )
        {
            turn(17);   
        } // end if
    } // end method turnAtEdge

    /**
     * Make the lobster turn, at random, ten percent of the time.
     * When the lobster turns, it will turn by some random angle
     * between -45 degrees and +45 degrees
     */
    public void randomTurn()
    {
        if (Greenfoot.getRandomNumber(100) < 10)
        {
            turn( Greenfoot.getRandomNumber(91) - 45); 
        }//end if
    } // end method randomTurn
} // end class Lobster
