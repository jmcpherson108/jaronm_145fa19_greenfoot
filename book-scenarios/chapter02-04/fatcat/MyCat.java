import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * MyCat is your own cat. Get it to do things by writing code in its act method.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyCat extends Cat
{
    private boolean tired = false;
    private boolean bored = true;
    /**
     * Act - do whatever the MyCat wants to do.
     */
    public void act()
    { 
        walkLeft(6);
        walkRight(3);
        eat();   
        if (bored == true)
        {
            dance();
        } // end if
        shoutHooray();
        if ( isSleepy() )
        {
            sleep(2);
        } // end if
        if ( isAlone() )
        {
            sleep(2);
        } // end if

        if (hasCompany() )
        {
            shoutHooray();
        }
    }     // end method act
} // end class MyCat
