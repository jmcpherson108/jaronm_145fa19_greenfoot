import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class TitleScreen here.
 * 
 * @author jaronm@email.uscb.edu
 * @version 145fa19 Final Project
 */
public class TitleScreen extends World
{

    /**
     * Constructor for objects of class TitleScreen.
     * 
     */
    public TitleScreen()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
    } // end TitleScreen constructor
    
    public void act()
    {
        if ( Greenfoot.mouseClicked( this) )
        {
            AcreWorld acreworld = new AcreWorld();
            Greenfoot.setWorld( acreworld );
        } // end if
    } // end method act
} // end class TitleScreen
