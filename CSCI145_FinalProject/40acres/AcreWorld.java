import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The 40 Acre world that you are in charge of
 * 
 * @author jaronm@email.uscb.edu
 * @version 145fa19 Final Project Assignment
 */
public class AcreWorld extends World
{
    public int cropsGathered;
    
    private int timeCounter; // keeps track of time in the world

    private Beetle[] beetles = new Beetle [2]; // an empty 1-D array 
    //of 2 beetles
    private int newCropTimer; // spawns more crops over time.

    private int[] beetleXcoords = { 2, 3}; // holds the x coordinates for the
                                           // two beetles

    private int[] beetleYcoords = { 2, 5}; // holds the y coordinates for the
                                           // two beetles

    /**
     * Create a new world with 8x5 cells with a cell size of 100x100 pixels. 
     * 
     */
    public AcreWorld()
    {    
        // Create a new world with 8x5 cells with a cell size of 100x100 pixels.
        super(8, 5, 100); 
        setBackground("grasstile.png"); 
        setPaintOrder(Mule.class,Beetle.class,Crop.class); // puts the Mule before the crop
        timeCounter = 2000;
        prepare();
    }
    
    /**
     * Processes for AcreWorld to carry out while the game is running
     */
    public void act()
    {
        timeCounter = timeCounter -= 1;

        showText("Time left: " + timeCounter, 4, 0);
        if (timeCounter <= 0)
        {
            showText (  "Time's up!", 4, 3 );
            Greenfoot.playSound("Alert.mp3");
            Greenfoot.stop();
        } // end if statement

        spawnNewCropTimer();
    } // end method act

    /**
     * spawns new crops into the world after a set amount of time
     */
    private void spawnNewCropTimer()
    {
        newCropTimer = (newCropTimer+= 1)%200;
        if (newCropTimer == 0) randomCrops(5);
    } // end method runNewCropTimer
    // spawnNewCropTimer code is derived from https://www.greenfoot.org/topics/5270

    /**
     * Places our good friend the Mule within the world (and the two beetles)
     */
    private void prepare()
    {
        Mule mule = new Mule();
        addObject(mule,4,2);

        int cropIndex = 0; // initialize counter variable
        int numberOfCrops = 6; // number of iterations

        randomCrops(5);

        beetles[0] = new Beetle();
        addObject(beetles[0], beetleXcoords[0], beetleYcoords[0]);
        beetles[1] = new Beetle();
        addObject(beetles[1], beetleXcoords[1], beetleYcoords[1]);
    } // end method prepare

    /**
     * spawns a set of crops into the world with the amount of crops being 
     * specified above
     */
    public void randomCrops(int howMany)
    {
        for (int i=0; i<howMany; i++) {
            Crop crop = new Crop();
            int x = Greenfoot.getRandomNumber(getWidth());
            int y = Greenfoot.getRandomNumber(getHeight());
            addObject(crop, x, y);
        }
    } // end method randomCrops
    
} // end class AcreWorld
