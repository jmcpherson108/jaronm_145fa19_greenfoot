import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A Beetle. Beetles just so happen to eat crops. Collect the crops
 * before they do
 * 
 * @author jaronm@email.uscb.edu
 * @version 145fa19 Final project
 */
public class Beetle extends Actor
{
    private GreenfootImage image1;
    
    public Beetle()
    {
        image1 = new GreenfootImage ( "Beetle.png" );
        
        setImage( image1 ); // the starting image for the beetle
    } // end Beetle constructor
    
    /**
     * Act - do whatever the Beetle wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        turnAtEdge();
        randomTurn();
        lookForCrop();
    } // end method act
    
    /**
     * Causes the Beetle to turn away when it is at the edge of the screen
     */
    public void turnAtEdge()
    {
        if ( isAtEdge() )
        {
            turn (25);
        } // end if statement
    } // end method turnAtEdge
    
    /**
     * causes the Beetle to randomly turn a set number of degrees 
     */
    public void randomTurn()
    {
        if (Greenfoot.getRandomNumber(100) < 25)
        {
            setLocation(getX()+1,getY());
            if (Greenfoot.getRandomNumber(100) < 10)
            {
                turn(Greenfoot.getRandomNumber(3)-2);
            setLocation(getX(),getY()-4);
            }
            if (Greenfoot.getRandomNumber(100) < 20)
            {
            setLocation(getX(),getY()+3);
            }
        } // end if
    } // end method randomTurn
    
    /**
     *  Checks to see whether the beetle has encountered a crop before the Mule could save it.
     */
    public void lookForCrop()
    {
        if ( isTouching(Crop.class) )
        { 
            removeTouching(Crop.class);
        } // end if
    } // end method lookForCrop
} // end class Beetle