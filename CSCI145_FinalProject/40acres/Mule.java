import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Mule. The offspring of a donkey and a horse. Sadly, the sprite is probably 
 * just a horse. Goes around collecting crops
 * 
 * @author jaronm@email.uscb.edu
 * @version 145fa19 Final Project
 */
public class Mule extends Actor
{
    public int cropsGathered;
    private GreenfootImage image1; 
    public Mule()
    {
        cropsGathered = 0;
        image1 = new GreenfootImage("mule.png");
        setImage(image1);
    } // end Mule constructor

    /**
     * Act - do whatever the Mule wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        checkKeyPress();
        lookForCrop();
    }    // end method act

    /**
     * Moves the mule one step forward
     */
    public void checkKeyPress()
    {
        String key = Greenfoot.getKey();
        if("right".equals(key)){
            setLocation(getX()+1,getY());
        }
        if("left".equals(key)){
            setLocation(getX()-1,getY());
        }
        if("down".equals(key)){
            setLocation(getX(),getY()+1);
        }
        if("up".equals(key)){
            setLocation(getX(),getY()-1);
        }
    } // end method checkKeyPress
    // code derived from https://www.greenfoot.org/topics/17

    /**
     * Does the same thing the beetle does, but better.
     */
    public void lookForCrop()
    {
        if ( isTouching(Crop.class) )
        { 
            removeTouching(Crop.class);
            
            cropsGathered =  cropsGathered +1; 
        } // end if
    } // end method lookForCrop
} // end class Mule

